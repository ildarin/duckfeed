
using DuckFeeding;

/* This is the nugget package we need */
using Microsoft.Extensions.DependencyInjection;

/* This is what happens in Startup class */
var provider = new ServiceCollection()
    .AddTransient<List<IFood>>()
    .AddScoped<Duck>()

    /* Factory method for getting instance of Duck in scope */
    .AddTransient<IFeed>(s => s.GetRequiredService<Duck>())
    .AddTransient<IDuck>(s => s.GetRequiredService<Duck>())
    .AddTransient<DuckFeeder>()
    .BuildServiceProvider();

/* Sasha feeds duck in main scope */
var sasha = provider.GetRequiredService<DuckFeeder>();
sasha.Feed(new Milk());

sasha.Ask();/* 1. Milk */

/* Mia & Riley feeds duck in second scope */
using (var duckFeedScope = provider.CreateScope())
{
    var mia = duckFeedScope.ServiceProvider.GetRequiredService<DuckFeeder>();
    mia.Feed(new Apple());
    mia.Ask();/* 2. Apple */

    var riley = duckFeedScope.ServiceProvider.GetRequiredService<DuckFeeder>();
    riley.Feed(new Banana());
    riley.Ask();/* 2. Apple, Banana */

    /* Scope dont matters for Sasha, she still feeds her scope duck */
    sasha.Feed(new Milk());
    sasha.Ask();/* 1. Milk,Milk */
}

//  Result output
//  #1. Milk { }                    <- Sasha
//  #2. Apple { }                       <- Mia, From scope #2
//  #2. Apple { }, Banana { }           <- Riley, From scope #2
//  #1. Milk { }, Milk { }           <- Sasha, from scope #1
//  Disposed:#2. Count: 2               <- Disposed, From scope #2

/* Implementation */
namespace DuckFeeding
{
    internal record DuckFeeder(IFeed Feeded, IDuck Duck)
    {
        public void Feed(IFood food) => Feeded.Eat(food);
        public void Ask() => Duck.Quack();
    }

    public interface IFeed { void Eat(IFood food); }
    public interface IFood { }
    internal record Apple() : IFood;
    internal record Banana() : IFood;
    internal record Milk() : IFood;

    public interface IDuck { void Quack(); }
    internal record Duck(List<IFood> Foods) : IDuck, IFeed, IDisposable
    {
        private bool _disposed = false;
        private static uint Id { get; set; }
        private string Name { get; } = ++Id + ".";
        public void Quack() => Console.WriteLine("#{0} {1}", Name, string.Join(", ", Foods));
        public void Eat(IFood food) => Foods.Add(food);
        public void Dispose()
        {
            if (_disposed) 
            { 
                return;
            }
            Console.WriteLine("Disposed:#{0} Count: {1}", Name, Foods.Count);
            Foods.Clear();
            _disposed = true;
        }
    }
}

namespace ScopeExample
{
    internal record DuckScopeFactory : IServiceScopeFactory
    {
        public IServiceScope CreateScope()
        {
            Console.WriteLine("Scope!");
            return new DuckScope(new DuckServiceProvider(new(new())));
        }
    }

    internal record DuckScope(IServiceProvider ServiceProvider) : IServiceScope
    {
        public void Dispose() { }
    }

    internal record DuckServiceProvider(Duck Duck) : IServiceProvider, IDisposable
    {
        public object? GetService(Type serviceType)
            => Duck;
        public void Dispose() => Duck.Dispose();
    }
}
